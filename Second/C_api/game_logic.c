
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "var.c"


bool correct(int *arr,int *main_array,int *word_array,int loop_index,int size_word,int pos){
    *(arr+0) = pos;
    for(int i = 0; i<size_word;i++){
        *(arr+i+1)= abs(pos-MOVES[loop_index]);
        if(*(word_array+i) != *(main_array+pos)){
            return false;
        }
        pos = abs(pos-MOVES[loop_index]);
    }
    return true;
}

int *Game(int *Ptr,int size,int *Word_Pointer,int word_size){
    for(int i=0;i<TOTAL_ARRAY_SIZE;i++){
        if(*(Ptr+i)==Word_Pointer[0]){
            // printf("%d ",i);
            int j = 0;
            while(j<8){
                int *positions = malloc((word_size)*sizeof(int));   
                bool TRUe = correct(positions,Ptr,Word_Pointer,j,word_size,i);
                if(TRUe){
                    return positions;
                }else{
                    free(positions);
                }
                j++;
            }
        }else{
            continue;
        }
    }
    int *Not_found = calloc(1,sizeof(int));
    Not_found[0] = -10;
    return Not_found;
}