#include "game_logic.c"


void Print_Matrix(int *PTR){
    for(int i=0;i<TOTAL_ARRAY_SIZE;i++){
        if(i%11==0){
                printf("\n");
            }
        printf("%c ",*(PTR+i));
    }
    printf("\n");
}

void print_done_matrix(int *find,int *PTR,int word_size){
    if(*(find+0)==-10){
        printf("The Word Not Found \n");
    }else{
        for(int i=0;i<TOTAL_ARRAY_SIZE;i++){
            if(i%11==0){
                printf("\n");
            }
            int f = 1;
            for(int j = 0;j<word_size;j++){
                if(*(find+j)==i){
                    printf("%c ",*(PTR+i));
                    f = 0;
                }
            }if(f){printf("- ");}
        }
        printf("\n");
    }
}

void main_game_fun(int *Game_array,int *W_R_D, int word_size){
    // Print_Matrix(Game_array);
    int *vv= Game(Game_array,TOTAL_ARRAY_SIZE,W_R_D,word_size);
    
    if(*(vv+0)==-10){
        printf("\nThe Word Not Found \n");
    }else{
        print_done_matrix(vv,Game_array,word_size);
    }
    free(vv);
    printf("\n");

}

bool main_game_fun_return_index(int *Game_array,int *W_R_D, int word_size,int *out_put){
    int *vv= Game(Game_array,TOTAL_ARRAY_SIZE,W_R_D,word_size);
    if(*(vv+0)==-10){
        printf("\nThe Word Not Found \n");
    }else{
        for(int i=0;i<word_size;i++){
            *(out_put+i) = *(vv+i);
        }
        free(vv);
        return 1;
    }
    free(vv);
    return 0;
}