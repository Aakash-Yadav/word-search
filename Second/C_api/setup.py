from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("C_word_search.pyx")
)

from os import listdir,system
for i in listdir('.'):
    if i == 'C_word_search.c' or i== 'build':
        system('rm -rf %s'%(i))
