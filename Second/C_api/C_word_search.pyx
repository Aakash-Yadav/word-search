from cpython cimport array
from ctypes import c_int ,CDLL

Clib = CDLL("./C_api.so");

cdef convert_string_array_to_int(list ar):
    cdef short int x;
    return array.array('i',[ord(ar[x]) for x in range(len(ar))]);

cpdef game_solver_print(list word_array, list word_list_to_search):

    cdef short int i = 0;
    cdef short int x;
    cdef array.array word_array_to_int = convert_string_array_to_int(word_array);
    cdef array.array word_array_to_search;

    arr = (c_int * len(word_array_to_int))(*word_array_to_int);
    Clib.Print_Matrix(arr)
    
    print(" ");

    for i in range(len(word_list_to_search)):
        word_array_to_search = convert_string_array_to_int([word_list_to_search[i][x] for x in range(len(word_list_to_search[i]))]);
        arr_2 = (c_int * len(word_array_to_search))(*word_array_to_search);
        Clib.main_game_fun(arr,arr_2,len(word_array_to_search));
    
    return 1;

cpdef search_and_return_index_array(list word_array, str word_to_search):

    cdef short int i = 0;
    cdef short int x;
    cdef array.array word_array_to_int = convert_string_array_to_int(word_array);
    cdef array.array result_array = array.array('i',[0 for x in range(len(word_to_search))])

    cdef array.array word_array_to_search = convert_string_array_to_int([word_to_search[x] for x in range(len(word_to_search))]);

    arr = (c_int * len(word_array_to_int))(*word_array_to_int);
    arr_2 = (c_int * len(word_array_to_search))(*word_array_to_search);
    word_index = (c_int * len(result_array))(*result_array);

    cdef int TRUE = Clib.main_game_fun_return_index(arr,arr_2,len(word_to_search),word_index);
    if(TRUE):
        return [x for x in word_index];
    else:
        return [0 for x in range(len(word_to_search))];