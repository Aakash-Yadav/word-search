
from cpython cimport array

cpdef binary_search_float(array.array data, int low , int high ,float item):
    cdef int mid 
    if low <= high :
        mid = (low+high)//2
        if data[mid]==item:
            return mid
        elif item< data[mid]:
            return binary_search_float(data,low,mid-1,item)
        else:
            return binary_search_float(data,mid+1,high,item)
    else:
        return -1


cdef factors_(int x,int c=1,array.array v=array.array('i',[])):
        if c == x: return v
        else:
            if x%c == 0 and c>1: v.append(c)
            return factors(x,c+1)

cdef tow_product_algo_(array.array n, int value):
    cdef int i,j 
    for i in range(len(n)):
        for j in range(i+1,len(n)):
            if n[i]*n[j] == value:
                return [n[i],n[j]]


cdef getting_ratio_of_the_two_words_(list data,int Max):
    cdef int Length = len(data)
    cdef int i , j ,povit,sub,k,sub2,n
    cdef list index = []
    if Length > 2:
        for i in data[0]:
            povit = i 
            for j in data[1]:
                sub = abs(povit-j)
                if sub==Max or sub==Max-1 or sub==Max+1  or sub==1 or sub==-1:
                    n = j
                    for k in data[2]:
                        sub2 = abs(n-k)
                        if sub2==Max or sub2==Max-1 or sub2==Max+1 or sub2==1 or sub2==-1 and sub<0:
                            index.append([povit,n,k])
        return index
    else:
        return False
    


cdef find_the_ratio_(int number):
    cdef array.array fac = factors_(number)
    cdef array.array SQRT_DATA = array.array('i',[(x*x) for x in fac])

    cdef int Find = binary_search_float(SQRT_DATA,0,len(SQRT_DATA)-1,float(number))

    if Find:return [array.array('i',[fac[Find],fac[Find]])]
    
    cdef list X = []
    cdef int i 
    cdef list value 
    
    for i in range(len(fac)):
        value = tow_product_algo_(fac , number)
        if value:        
            fac.remove(min(value))
            X.append(value)
    return X


cdef common_factor_meen(list value_,str word):
    
    cdef list one = []

    
    cdef list DD = []
    cdef int i,j,k,M

    for DD in value_:
        x = array.array('i',[])
        for i in range(len(DD)-1):
            j = DD[i]
            k = DD[i+1]
            M = abs(j-k)
            x.append(M)
        one.append(x)

    return one 


cpdef common_factor_meen_(list value_,str word):
    return common_factor_meen(value_,word)

 

cpdef find_the_ratio(int number):
    return find_the_ratio_(number)

cpdef tow_product_algo(array.array n, int value):
    return tow_product_algo_( n, value)

cpdef factors(int x,int c=1,array.array v=array.array('i',[])):
    return factors_(x, c, v)

cpdef getting_ratio_of_the_two_words(list data,int Max):
    return getting_ratio_of_the_two_words_(data, Max)