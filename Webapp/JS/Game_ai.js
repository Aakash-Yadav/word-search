


const SIZE = 121;

const LEFT = -1;
const RIGHT = 1;

const UPPER = -11;
const LOWER = 11;

const LEFT_UPPER_DIAGONAL = -12;
const LEFT_DOWN_DIAGONAL =   10;

const RIGHT_UPPER_DIAGONAL = -10;
const RIGHT_DOWN_DIAGONAL =   12;

const MOVES = [UPPER,LOWER,LEFT,RIGHT,LEFT_DOWN_DIAGONAL,LEFT_UPPER_DIAGONAL,RIGHT_DOWN_DIAGONAL,RIGHT_UPPER_DIAGONAL]



// async function SEARCH_FUN(game_board,Word,Move,Pos){
//     let out = [];
//     out.push(Pos);
//     for(let i = 0 ; i<Word.length ; i++){
//         if(Word[i]!=game_board[Pos]){
//             console.log(out)
//                 let db = document.getElementById(`game_id${Pos}`);
//                 await sleep(10);
//                 db.style.color = '#50fa7b';
//                 db.style.backgroundColor = "black"

//             return 0;
//         }else{
//         out.push(Math.abs(Pos-MOVES[Move]));
//         }
//         Pos = Math.abs(Pos-MOVES[Move]);
//     }

//     for(let i=0;i<out.length;i++){
//         let db = document.getElementById(`game_id${out[i]}`);
//         db.style.color  =hexColor()
//         db.style.backgroundColor = hexColor()
//         await sleep(200)
//     }
//     return out;
// }

function SEARCH_FUN(game_board,Word,Move,Pos){
    let out = [];
    out.push(Pos);
    for(let i = 0 ; i<Word.length-1 ; i++){
        if(Word[i]!=game_board[Pos]){
            return 0;
        }else{
        out.push(Math.abs(Pos-MOVES[Move]))
        }
        Pos = Math.abs(Pos-MOVES[Move]);
    }
    return out;
}


async function word_search_ai(game_board,word_to_search){

    for(let i=0; i<SIZE; i++){
        
        let db = document.getElementById(`game_id${i+1}`);
        if(db.style.color!="black"){
            db.style.color  =hexColor()
            db.style.backgroundColor = hexColor()
            await sleep(50)
            db.style.color = '#50fa7b';
            db.style.backgroundColor = "black"
        }

        if(game_board[i]==word_to_search[0]){
            
            let move = MOVES.length;
            
            while(move>0){
                
                let data_find = SEARCH_FUN(game_board,word_to_search,move,i);
                if(data_find){

                    for(let j = 0; j<data_find.length;j++){
                        let db = document.getElementById(`game_id${data_find[j]+1}`);
                        db.style.color  = "black"
                        db.style.backgroundColor = "white"
                        await sleep(30)
                        // db.style.color = '#50fa7b';
                        // db.style.backgroundColor = "black"
                    }

                    

                }
                move--;
            }
        }

    }
}


let game_screen_2 = [
    'H', 'Y', 'P', 'Y', 'F', 'L', 'U', 'I', 'D', 'S', 'E',
    'S', 'P', 'I', 'E', 'L', 'A', 'N', 'F', 'A', 'Y', 'N',
    'R', 'A', 'N', 'V', 'S', 'I', 'L', 'K', 'C', 'N', 'I', 
    'O', 'F', 'V', 'I', 'S', 'K', 'D', 'S', 'Y', 'U', 'L', 
    'T', 'D', 'I', 'S', 'G', 'U', 'Y', 'U', 'I', 'T', 'Y', 
    'S', 'Y', 'T', 'S', 'A', 'N', 'Y', 'D', 'O', 'F', 'B', 
    'T', 'M', 'E', 'A', 'H', 'T', 'I', 'M', 'S', 'L', 'Y', 
    'A', 'E', 'D', 'P', 'E', 'L', 'T', 'S', 'U', 'B', 'C', 
    'R', 'O', 'H', 'M', 'S', 'R', 'T', 'D', 'O', 'T', 'S', 
    'T', 'G', 'N', 'I', 'T', 'S', 'O', 'P', 'A', 'L', 'L', 
    'T', 'U', 'N', 'K', 'I', 'N', 'D', 'N', 'E', 'S', 'S'
]
list_wrd =['BUSTLE', 'BYLINE', 'CLOUDILY', 'DOTS', 'DYNASTY', 'FALSIFY', 'FLUIDS', 'IMPASSIVE', 'INVITED', 'LOSING', 'PESKY', 'POSTING', 'SMITH', 'START', 'UNKINDNESS']

// word_search_ai(game_screen_2,'BUSTLE')
// word_search_ai(game_screen_2,'BYLINE')
// word_search_ai(game_screen_2,'CLOUDILY')
// (word_search_ai(game_screen_2,list_wrd[1]))

for(let i = 0; i<list_wrd.length;i++){
    (word_search_ai(game_screen_2,list_wrd[i]))
}

