function Game_ai() {
  const new_value = [];
  const new_value_2 = [];
  const SIZE = 121;

  const LEFT = -1;
  const RIGHT = 1;

  const UPPER = -11;
  const LOWER = 11;

  const LEFT_UPPER_DIAGONAL = -12;
  const LEFT_DOWN_DIAGONAL = 10;

  const RIGHT_UPPER_DIAGONAL = -10;
  const RIGHT_DOWN_DIAGONAL = 12;

  const MOVES = [
    UPPER,
    LOWER,
    LEFT,
    RIGHT,
    LEFT_DOWN_DIAGONAL,
    LEFT_UPPER_DIAGONAL,
    RIGHT_DOWN_DIAGONAL,
    RIGHT_UPPER_DIAGONAL,
  ];
  const value = document.getElementsByClassName("cellVal11");
  const words_data = document.getElementById("words");

  function SEARCH_FUN(game_board, Word, Move, Pos) {
    const out = [];
    out.push(Pos);
    for (let i = 0; i < Word.length - 1; i++) {
      if (Word[i] != game_board[Pos]) {
        return 0;
      } else {
        out.push(Math.abs(Pos - MOVES[Move]));
      }
      Pos = Math.abs(Pos - MOVES[Move]);
    }
    return out;
  }

  function word_search_ai(game_board, word_to_search) {
    for (let i = 0; i < SIZE; i++) {
      if (game_board[i] == word_to_search[0]) {
        let move = MOVES.length;
        while (move > 0) {
          let data_find = SEARCH_FUN(game_board, word_to_search, move, i);
          if (data_find) {
            for(let i = 0;i<data_find.length;i++){
                value[data_find[i]].style.color = "red"
            }
          }
          move--;
        }
      }
    }
  }

  

  const Words_Length = words_data.childNodes.length;
  const Words_Content_l = value.length;

  for (let i = 0; i < Words_Content_l; i++) {
    new_value.push(value[i].innerText);
  }

  for (let i = 0; i < Words_Length; i++) {
    new_value_2.push(words_data.childNodes[i].innerText);
  }

//   word_search_ai(new_value, new_value_2[0]);
    for(let i= 0 ; i<new_value_2.length;i++ ){
        word_search_ai(new_value, new_value_2[i]);
    }
}

