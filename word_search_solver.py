
""" IMPORT """
from array import array 
from functools import reduce
from string import ascii_uppercase
from concurrent.futures import ProcessPoolExecutor
from json import loads
from math import sqrt
from basic_algo import (factors,tow_product_algo,find_the_ratio,
                        getting_ratio_of_the_two_words,common_factor_meen_,
                        binary_search_float)
from collections import ChainMap

Mini_fun = lambda n : ascii_uppercase.index(n)
REDUCE_FUN = lambda a,b : a-b 

def Load_Json_File(path='data_sets.json'):
    with open(path,'r') as f:
        word_array = loads(f.read())
    return word_array['WORDS_2']


def convert_alphabet_to_number(word_array):
    return array('i',[Mini_fun(x) for x in word_array])

int_value_of_words = convert_alphabet_to_number(Load_Json_File())

def linear_search(value_w):
    value = Mini_fun(value_w)
    out = {value_w:[]}
    for i,alphabet in enumerate(int_value_of_words):
        if alphabet == value:
            (out[value_w].append(i))
        else:continue
    return out

array_of_words = Load_Json_File()

def Cross_check(Array,word,correct):
    DATA = dict(ChainMap(*[linear_search(x) for x in word]))
    if reduce(REDUCE_FUN,correct) is 0:
        add = correct[0]
        new_array = array('i',[])
        for i in range(len(word[:2])):
            if Array[0]<Array[-1] :
                Array.append(Array[-1]+add)
            else:
                Array.append(abs(Array[-1]-add))
        return_value = 0
        for k,i in enumerate(word):
            b= array('i',DATA.get(i))
            TRUE = binary_search_float(b,0,len(b)-1,Array[k])
            if TRUE>-1:
                new_array.append(Array[k])
        
        if len(word)==len(new_array):
            return new_array


def SOLVING_FUNC(MAX,WORD):
    word = list(WORD)
    data = dict(ChainMap(*list(map(linear_search,word))))
    out_come = []
    for i in (WORD):
        out_come.append(data[i])
    x=(getting_ratio_of_the_two_words(out_come,MAX))
   
    if len(x) is 1:
        x = x[-1]
        SUB = True
        con,con_2 = x[-1],x[-2]
        value = abs(con-con_2)
        if x[-1]<x[-2]:
            pass
        else:
            SUB = False
        for inde in range(len(WORD[2:])-1):
            
            if SUB:
                new_index = x[-1]-value
            else:
                new_index = x[-1]+value
                
            if new_index>0:
                x.append(new_index)
        return x
    else:
        v=common_factor_meen_(x,WORD)
        if reduce(REDUCE_FUN,v[0]) is 0 and reduce(REDUCE_FUN,v[1]) is 0:
            return_val= []
            for i in range(len(v)):
                print('soxx')
                return_stat = Cross_check(x[i], WORD, v[i])
                if return_stat:
                    for b in return_stat:
                        return_val.append(b)
                else:
                    pass 
            return return_val
    
        ADD_SUB = None
        correct = None
        MEEN_EROR = []
        for i,j in enumerate(v):
            if  reduce(REDUCE_FUN,j)==0:
                correct = i
        add = v[correct][0]
    
        x = x[correct]
        result = []
        for i in range(len(WORD[2:])-1):
            if x[-1]<x[0]:
                ADD_SUB = 1
                x.append(x[-1]-add)
            else:
                x.append(x[-1]+add)
        return (x)

def MAIN_FUNCATION(word:str,i=0,RATIO=0):
    LENGTH = len(int_value_of_words)
    RATIO = (find_the_ratio(LENGTH))
    # print(RATIO)
    # print(len(RATIO))
    if len(RATIO) is 1:
        RATIO = RATIO[-1]
        MIN,MAX = min(RATIO),max(RATIO)
        return SOLVING_FUNC( MAX, word)
    else:
        RATIO_OF_ALL = sorted(RATIO)[::-1]
        
        FIRST_RATIO = RATIO_OF_ALL[i]
        
        MIN,MAX = min(FIRST_RATIO),max(FIRST_RATIO)
        return   SOLVING_FUNC( MAX, word)
        
        

def PLAY_BOT(word:str):
    return MAIN_FUNCATION(word)

cx = ['CAT', 'DOG', 'DONKEY', 'GOAT', 'HORSE', 'PIG', 'SHEEP', 'TURTLE']

v = list(map(PLAY_BOT,cx))
v=(sum([x for x in v],[]))
cc = Load_Json_File()
print(cc)
print('\n')


for i in range(len(cc)):
    if i in v:
        pass 
    else:
        cc[i]=''    

print(cc)