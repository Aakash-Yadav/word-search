// Online C compiler to run C program online
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define TOTAL_ARRAY_SIZE 121
#define UPPER  -11
#define LOWER  11
#define LEFT  -1
#define RIGHT  1
#define R_UPPER_DIAGONAL  -12
#define R_LOWER_DIAGONAL   12
#define L_UPPER_DIAGONAL  -10
#define L_LOWER_DIAGONAL   10

const int  MOVES[8] = {UPPER,LOWER,LEFT,RIGHT,R_UPPER_DIAGONAL,R_LOWER_DIAGONAL,L_UPPER_DIAGONAL,L_LOWER_DIAGONAL};

bool correct(int *arr,int *main_array,int *word_array,int loop_index,int size_word,int pos){
    // printf("%d \n",loop_index);
    *(arr+0) = pos;
    for(int i = 0; i<size_word;i++){
        *(arr+i+1)= abs(pos-MOVES[loop_index]);
        if(*(word_array+i) != *(main_array+pos)){
            return false;
        }
        // printf("%c-%c %d ",*(word_array+i),*(main_array+pos), pos);
        pos = abs(pos-MOVES[loop_index]);
    }
    return true;
}
int *Game(int *Ptr,int size,int *FF,int word_size){
    for(int i=0;i<TOTAL_ARRAY_SIZE;i++){
        if(*(Ptr+i)==FF[0]){
            // printf("%d ",i);
            int j = 0;
            while(j<8){
                int *v = malloc((word_size)*sizeof(int));   
                bool TRUe = correct(v,Ptr,FF,j,word_size,i);
                if(TRUe){
                    return v;
                }else{
                    free(v);
                }
                j++;
            }
        }else{
            continue;
        }
    }
    int *Not_found = calloc(1,sizeof(int));
    Not_found[0] = -10;
    return Not_found;
}

void Print_Matrix(int *PTR){
    for(int i=0;i<TOTAL_ARRAY_SIZE;i++){
        if(i%11==0){
                printf("\n");
            }
        printf("%c ",*(PTR+i));
    }
    printf("\n");
}

void print_done_matrix(int *find,int *PTR,int word_size){
    if(*(find+0)==-10){
        printf("The Word Not Found \n");
    }else{
        for(int i=0;i<TOTAL_ARRAY_SIZE;i++){
            if(i%11==0){
                printf("\n");
            }
            int f = 1;
            for(int j = 0;j<word_size;j++){
                if(*(find+j)==i){
                    printf("%c ",*(PTR+i));
                    f = 0;
                }
            }if(f){printf("- ");}
        }
    }
}

int main() {
    int *PTR;
    int value[TOTAL_ARRAY_SIZE] = {'A', 'N', 'R', 'L', 'Z', 'H', 'U', 'T', 'S', 'I', 'W', 'J', 'D', 'E', 'H', 'S', 'I', 'N', 'O', 'T', 'S', 'A', 'A', 'G', 'A', 'M', 'A', 'O', 'S', 'M', 'G', 'Y', 'A', 'Z', 'N', 'S', 'P', 'A', 'W', 'W', 'A', 'T', 'F', 'N', 'Z', 'I', 'D', 'Y', 'T', 'V', 'W', 'I', 'F', 'G', 'G', 'Y', 'M', 'A', 'G', 'O', 'E', 'R', 'O', 'O', 'R', 'I', 'H', 'A', 'L', 'L', 'M', 'A', 'R', 'K', 'N', 'U', 'S', 'Y', 'R', 'N', 'J', 'G', 'D', 'R', 'S', 'I', 'E', 'N', 'I', 'F', 'Z', 'L', 'G', 'R', 'V', 'K', 'H', 'L', 'O', 'C', 'A', 'U', 'S', 'E', 'H', 'C', 'I', 'R', 'I', 'C', 'J', 'V', 'E', 'C', 'A', 'P', 'S', 'R', 'I', 'A', 'M'};
    
    // int W_R_D[] = {'A','F','F','O','R','D'};
    int W_R_D[] = {'W' ,'I', 'S' ,'T'};
    // List of words to search 
    // ["ADAPTERS","AFFORD","AIRSPACE","ASTONISHED","CONSIGN","FRAMING"//,"GRUEL","HALLMARK","JAZZY","LADS","RICHES","SHIM//,"VULGARITY","WAGS","WIST"]
    
    int word_size = sizeof(W_R_D)/sizeof(W_R_D[0]);
    PTR = &value[0];
    Print_Matrix(PTR);
    int *vv= Game(PTR,TOTAL_ARRAY_SIZE,W_R_D,word_size);
    
    if(*(vv+0)==-10){
        printf("\nThe Word Not Found \n");
    }else{
        print_done_matrix(vv,PTR,word_size);
    }
    free(vv);
    printf("\n");
    return 0;
}
